---
title: Install
author: Duncan Macleod <duncan.macleod@ligo.org>
---

# Installation

MkDocs Material for IGWN can be installed from
[conda-forge](https://conda-forge.org):

```shell
conda install --channel conda-forge mkdocs-material-igwn
```

Alternatively, it can be installed using
[Pip](https://pip.pypa.io):

```shell
python -m pip install mkdocs-material-igwn
```
