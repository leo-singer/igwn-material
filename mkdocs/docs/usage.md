---
title: Usage
author: Duncan Macleod <duncan.macleod@ligo.org>
---

# Usage

To use MkDocs Material for IGWN, just specify
`name: material_igwn` in the `theme` section of your `mkdocs.yml`:

```yaml
theme:
  name: material_igwn
```

For a full example, see the `mkdocs.yml` for this site:

```yaml
{!mkdocs.yml!}
```
