# IGWN Material Documentation themese

This repo defines linked themes for IGWN documentation based on 
[MkDocs for Material](https://squidfunk.github.io/mkdocs-material/).

The following theme packages are provided:

| Package | Platform | Documentation |
| ------- | -------- | ------------- |
| `mkdocs-material-igwn` | [MkDocs](https://www.mkdocs.org) | <https://computing.docs.ligo.org/igwn-material/mkdocs/> |
| `sphinx-immaterial-igwn` | [Sphinx](https://www.sphinx-doc.org) | <https://computing.docs.ligo.org/igwn-material/sphinx/> |
