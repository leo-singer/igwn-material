#####
Usage
#####

To use Sphinx-Immaterial-IGWN just specify ``sphinx_immaterial_igwn``
in your ``conf.py`` file as follows:

.. code-block:: python

    extensions = ["sphinx_immaterial_igwn"]
    html_theme = "sphinx_immaterial_igwn"

See `Customization <https://jbms.github.io/sphinx-immaterial/customization.html>`__
in the upstream Sphinx-Immaterial documentation for detailed
descriptions of how to customize the theme.

For a full example, see the ``conf.py`` file for this site:

.. literalinclude:: conf.py
   :language: python
